//
//  DetailViewController.swift
//  Bigthing3.0
//
//  Created by chenqianru123 on 22/11/19.
//  Copyright © 2019 chenqianru123. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var Dimageview: UIImageView!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var locationLb: UILabel!
    @IBOutlet weak var yearLb: UILabel!
    @IBOutlet weak var votesLb: UILabel!
    @IBOutlet weak var ratingLb: UILabel!
    @IBOutlet weak var MyratingLb: UILabel!
    @IBOutlet weak var rateLb: UILabel!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var descriptionLb: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    
    func configureView() {
        // Update the user interface for the detail item.
        
        if let detail = self.detailItem {
            if let name = self.nameLb {
                self.navigationItem.title = detail.name.description
                name.text = "Name:" + detail.name.description
            }
            if let location = self.locationLb{
                location.text = "Location: " + detail.location.description
            }
            if let votes = self.votesLb{
                votes.text = "Votes:" + detail.votes.description
            }
            if let year = self.yearLb{
                year.text = "Bulid Year: " + detail.year.description
            }
            if let rating = self.ratingLb{
                rating.text = "Rating: " + detail.rating.description
            }
            if let description = self.descriptionLb{
                description.text = detail.descri.description
            }
            if let image = self.Dimageview{
                image.image = detail.image
            }
            if let btn = self.favBtn{
                if( detail.favourite){
                    btn.setTitle("❤️" + "Saved", for: UIControl.State())
                }
                else{
                    favBtn.setTitle("Add to Favourites", for: UIControl.State())
                }
            }
            if let ratebtn = self.rateBtn{
                if(detail.rated){
                    ratebtn.isEnabled = false
                    ratebtn.setTitle("✭" + "Rated", for: UIControl.State())
                }
            }
        }
    }
    @IBAction func slider(_ sender: Any) {
        let currentValue  = String(Int(self.slider.value))
        rateLb.text = currentValue}
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.configureView()
    }
    
    var detailItem: Bigthing? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func rateButton(_ sender: Any) {
        rateBtn.setTitle("✭" + "Rated", for: UIControl.State())
        rateBtn.isEnabled = false
        DataManager.sharedDataManager.submitRating(id: detailItem!.id, rating: String(Int(slider.value)))
        DataManager.sharedDataManager.updateCoreData(bigthing: self.detailItem!,key: "rated",value: true)
    }
    
    @IBAction func FavBtnClicked(_ sender: Any) {
        if (self.detailItem!.favourite) {
            DataManager.sharedDataManager.updateCoreData(bigthing: self.detailItem!,key: "favourite",value: false)
            self.detailItem!.favourite = false
        }
        else{
            DataManager.sharedDataManager.updateCoreData(bigthing: self.detailItem!,key: "favourite",value: true)
            self.detailItem!.favourite = true
            
        }
        let heart = "❤️" + "Saved"
        let heart2 =  "Add to Favourites"
        
        favBtn.setTitle(heart2, for: UIControl.State())
        if(self.detailItem!.favourite){
            favBtn.setTitle(heart, for: UIControl.State())
            
        }
      
    }
    
}

