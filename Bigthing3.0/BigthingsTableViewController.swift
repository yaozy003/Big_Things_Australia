//
//  BigthingsTableViewController.swift
//  Bigthing
//
//  Created by chenqianru123 on 21/11/19.
//  Copyright © 2019 chenqianru123. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class BigthingsTableViewController: UIViewController {
    var bigThings:[Bigthing] = []
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self as UITableViewDataSource
        tableView.delegate = self as UITableViewDelegate
      
        bigThings = DataManager.sharedDataManager.bigthings
        print(bigThings[0])
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
//func createArray(){
//    let urlStr = "http://partiklezoo.com/bigthings/?"
//        let url = URL.init(string: urlStr)
//        let request = URLRequest.init(url: url!)
//        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
//
//            if let tmpData = data {
//                if let str =  String(data:tmpData ,encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
//                    let jsonData:Data = str.data(using: .utf8)!
//                    let arr:[Dictionary<String, String>] = try! JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! [Dictionary<String, String>]
//                        for dic in arr {
//                            print(dic)
//                            let image = dic["image"]
//                            let name = dic["name"]
//                            //Coredata.insertFish(name!, image!)
//                            //self.fishArray = Coredata.fetchAllFish()
//                            let app = UIApplication.shared.delegate as! AppDelegate
//                            let context = app.persistentContainer.viewContext
//                            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Bigthing")
//                            let tmpFish = NSEntityDescription.insertNewObject(forEntityName: "Bigthing", into: context) as! Bigthing
//                            tmpFish.image = image
//                            tmpFish.name = name
//                            do {
//                                try context.save()
//                            } catch {
//                                fatalError("save error：\(error)")
//                            }
//                            do {
//                                self.bigThings = try context.fetch(fetchRequest) as! [Bigthing]
//                            }
//                            catch {
//                                fatalError("error：\(error)")
//                            }
//                            DispatchQueue.main.sync {
//                                self.tableView.reloadData()
//                            }
//                        }
//                }
//            }
//        }
//        task.resume()
//    }
//}
}
extension BigthingsTableViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bigThings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bt1 = self.bigThings[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "BTCell", for: indexPath) as! BigthingTableViewCell
        cell.setBigthing(Bt: bt1)
        return cell
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
//        vc.fish = self.fishArray[indexPath.row]
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
}

