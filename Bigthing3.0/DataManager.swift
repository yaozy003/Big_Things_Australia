

import UIKit
import CoreData
import Foundation
class DataManager {
    static let sharedDataManager = DataManager()//singleton
    var bigthings = [Bigthing]()
    var storedBigthings = [NSManagedObject]()
    var menu = [UIImage]()
    
    var favourites: [Bigthing] {
        get {
            var selectedBigthings = [Bigthing]()
            
            if (bigthings.count > 0)
            {
                for count in 0...bigthings.count - 1
                {
                    if bigthings[count].favourite
                    {
                        selectedBigthings.append(bigthings[count])
                    }
                }
            }
            
            return selectedBigthings
        }
    }
    
    init(){
        self.refreshBigthings()
        self.loadBigthings()
    }
    
    func refreshBigthings()  {
        let url = NSURL(string:"https://www.partiklezoo.com/bigthings/?")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: url! as URL, completionHandler:
        {(data, response, error) in
            if (error != nil) { return; }
            if let json = try? JSON(data: data!) {
                if json.count > 0 {
                    for count in 0...json.count - 1 {
                        let jsonBigthing = json[count]
                        let newBigthing = Bigthing(id: jsonBigthing["id"].string!,
                                                   name: jsonBigthing["name"].string!,
                                                   location: jsonBigthing["location"].string!,
                                                   year: jsonBigthing["year"].string!,
                                                   status: jsonBigthing["status"].string!,
                                                   latitude: jsonBigthing["latitude"].string!,
                                                   longitude: jsonBigthing["longitude"].string!,
                                                   rating: jsonBigthing["rating"].string!,
                                                   votes: jsonBigthing["votes"].string!,
                                                   updated: jsonBigthing["updated"].string!,
                                                   description: jsonBigthing["description"].string! )
                        let imageURLString = "http://partiklezoo.com/bigthings/images/" + jsonBigthing["image"].string!
                        self.addItemToBigthings(newBigthing,imageURL: imageURLString)
                    }
                }
            }
        })
        
        task.resume()
    }
    
    func loadBigthings() {
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bigthing")
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            storedBigthings = results as! [NSManagedObject]
            
            if (storedBigthings.count > 0)
            {
                for index in 0 ... storedBigthings.count - 1
                {
                    let id = storedBigthings[index].value(forKey: "id") as! String
                    let name = storedBigthings[index].value(forKey: "name") as! String
                    let location = storedBigthings[index].value(forKey: "location") as! String
                    let binaryData = storedBigthings[index].value(forKey: "image") as! Data
                    let image = UIImage(data: binaryData)
                    let year = storedBigthings[index].value(forKey: "year") as! String
                    let status = storedBigthings[index].value(forKey: "status") as! String
                    let latitude = storedBigthings[index].value(forKey: "latitude") as! String
                    let longitude = storedBigthings[index].value(forKey: "longitude") as! String
                    let rating = storedBigthings[index].value(forKey: "rating") as! String
                    let votes = storedBigthings[index].value(forKey: "votes") as! String
                    let updated = storedBigthings[index].value(forKey: "update") as! String
                    let favourite = storedBigthings[index].value(forKey: "favourite") as! Bool
                    let rated = storedBigthings[index].value(forKey: "rated") as! Bool
                    let description = storedBigthings[index].value(forKey: "descri") as! String
                    
                    let loadedBigthing = Bigthing(id: id, name: name, location: location, image: image!, year: year, status: status, latitude: latitude, longitude: longitude, rating: rating, votes: votes, updated:updated, description: description,favourite:favourite,rated:rated)
                    
                    bigthings.append(loadedBigthing)
                }
            }
        }
        catch let error as NSError
        {
            print("Could not load. \(error), \(error.userInfo)")
        }
    }
    
    func addItemToBigthings(_ newBigthing: Bigthing!, imageURL: String) {
        //check to see if the bigthing already exists
        if checkForBigthing(newBigthing) == -1 {
            
            //get a reference to the managed context
            //DispatchQueue.main.async {
            let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
            //call our loadImage function to create an image object from the url
            newBigthing.image = self.loadImage(imageURL)
            //get an entity reference and ManagedObject
            let entity = NSEntityDescription.entity(forEntityName: "Bigthing", in: managedContext)
            let bigthingToAdd = NSManagedObject(entity: entity!, insertInto: managedContext)
            
            //set the data for our new ManagedObject
            //bigthingToAdd.setValue(newbigthing.image, forKey: "image")
            
            bigthingToAdd.setValue(newBigthing.id, forKey: "id")
            bigthingToAdd.setValue(newBigthing.name, forKey: "name")
            bigthingToAdd.setValue(newBigthing.location, forKey: "location")
            bigthingToAdd.setValue(newBigthing.year!, forKey: "year")
            bigthingToAdd.setValue(newBigthing.status!, forKey: "status")
            bigthingToAdd.setValue(newBigthing.latitude!, forKey: "latitude")
            bigthingToAdd.setValue(newBigthing.longitude!, forKey: "longitude")
            bigthingToAdd.setValue(newBigthing.image!.pngData(), forKey: "image")
            bigthingToAdd.setValue(newBigthing.rating!, forKey: "rating")
            bigthingToAdd.setValue(newBigthing.votes!, forKey: "votes")
            bigthingToAdd.setValue(newBigthing.descri!, forKey: "descri")
            bigthingToAdd.setValue(newBigthing.update, forKey: "update")
            bigthingToAdd.setValue(false, forKey: "favourite")
            bigthingToAdd.setValue(false, forKey: "rated")
            //save managed context
            do {
                try managedContext.save()
            }
            catch let error as NSError
            {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
            
            //Add the ManagedObject to our array
            self.storedBigthings.append(bigthingToAdd)
            //}
            //Add the newbigthing to our bigthings array
            bigthings.append(newBigthing)
        }
        
    }
    
    func submitRating(id: String, rating: String){
        
        let myUrl = URL(string: "https://www.partiklezoo.com/bigthings/?");
        
        var request = URLRequest(url:myUrl!)
        
        request.httpMethod = "POST"// Compose a query string
        
        let postString = "action=rate&id=" + id + "&rating=" + rating
        print(postString)
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        print(request)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json {
                    print(parseJSON)
                }
            } catch {
                print(error)
            }
        }
        task.resume()
        
    }

    func updateCoreData(bigthing:Bigthing,key:String,value:Bool){
        var index = 0
        print("update core data")
        for item in DataManager.sharedDataManager.storedBigthings{
            
            if(bigthing.id == item.value(forKey: "id")as! String){
                break
            }
            index += 1
        }
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Bigthing")
        
        do {
            let test = try managedContext.fetch(fetchRequest)
            let obj = test[index] as! NSManagedObject
            obj.setValue(value,forKey:key)
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        }
        catch{
            print(error)
        }
    }
    
    
    func checkForBigthing(_ searchItem: Bigthing) -> Int {
        var targetIndex = -1
        
        if (bigthings.count > 0) {
            for index in 0 ... bigthings.count - 1 {
                if (bigthings[index].id.isEqual(searchItem.id)) {
                    targetIndex = index
                }
            }
        }
        
        return targetIndex
    }
    
    func loadImage(_ imageURL: String) -> UIImage
    {
        var image: UIImage!
        if let url = NSURL(string: imageURL)
        {
            if let data = NSData(contentsOf: url as URL)
            {
                image = UIImage(data: data as Data)
            }
        }
        return image!
    }
    
    
    
}
