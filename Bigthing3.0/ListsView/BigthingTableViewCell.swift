//
//  BigthingTableViewCell.swift
//  Bigthing
//
//  Created by chenqianru123 on 21/11/19.
//  Copyright © 2019 chenqianru123. All rights reserved.
//

import UIKit

class BigthingTableViewCell: UITableViewCell {

    @IBOutlet weak var BTimage: UIImageView!
    @IBOutlet weak var nameLb: UILabel!
    
    @IBOutlet weak var statusLb: UILabel!
    @IBOutlet weak var locationLb: UILabel!
   
    
    
    func setBigthing(Bt:Bigthing){
        self.BTimage.image = Bt.image
        self.nameLb.text = Bt.name
        self.statusLb.text = Bt.status
        self.locationLb.text = Bt.location
    }
    

    
}
