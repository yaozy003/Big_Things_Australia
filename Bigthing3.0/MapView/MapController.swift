//
//  MapController.swift
//  Bigthing3.0
//
//  Created by chenqianru520 on 26/11/19.
//  Copyright © 2019 chenqianru123. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
import UIKit



class MapController: UIViewController,MKMapViewDelegate{
    
    
    var pins:[MKAnnotation] = []
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        self.display()
        
    }
    

    class customPin: NSObject,MKAnnotation{
        var coordinate:CLLocationCoordinate2D
        var title:String?
        var subtitle:String?
        init(pinTitle:String,pinSubtitle:String,location:CLLocationCoordinate2D) {
              self.title = pinTitle
                  self.subtitle = pinSubtitle
                  self.coordinate = location
        }
    }
    func loadPins(){
        for item in DataManager.sharedDataManager.bigthings {
            var lat:CLLocationDegrees = CLLocationDegrees(item.latitude) as! CLLocationDegrees
            var long:CLLocationDegrees = CLLocationDegrees(item.longitude) as! CLLocationDegrees
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let pin = customPin(pinTitle: item.name, pinSubtitle: item.location, location: location)
            pins.append(pin)
        }
    }
 
    

    
    func display(){
       
        let region = MKCoordinateRegion(center:CLLocationCoordinate2D(latitude: -28.5636, longitude: 144.37339), span:MKCoordinateSpan(latitudeDelta: 28, longitudeDelta: 28))
        self.mapView.setRegion(region, animated: true)
       loadPins()
        self.mapView.addAnnotations(pins)
        self.mapView.delegate = self
    }
    
//    func mapView(_ mapView: MKMapView, viewFor annotation:MKAnnotation)->MKAnnotationView? {
//        if annotation is MKUserLocation{ return nil}
//        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "customannotation")
//        annotationView.canShowCallout = true
//        return annotationView
//    }
    
    
}
