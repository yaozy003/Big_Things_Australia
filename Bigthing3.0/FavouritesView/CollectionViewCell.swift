//
//  CollectionViewCell.swift
//  Bigthing3.0
//
//  Created by chenqianru520 on 29/11/19.
//  Copyright © 2019 chenqianru123. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var CellImageView: UIImageView!
    @IBOutlet weak var CellLabel: UILabel!
    
    func setCell(Bt:Bigthing){
        self.CellImageView.image = Bt.image
        self.CellLabel.text = Bt.name
        print("image:" + "\(self.CellImageView.image)")
        print("text:" + "\(self.CellLabel.text)")
    }
}
