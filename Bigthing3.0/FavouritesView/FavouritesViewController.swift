//
//  FavouritesViewController.swift
//  Bigthing3.0
//
//  Created by chenqianru520 on 28/11/19.
//  Copyright © 2019 chenqianru123. All rights reserved.
//

import UIKit

class FavouritesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {


  
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.dataSource = self
        self.collectionView!.delegate = self
         self.collectionView!.reloadData()
        // Do any additional setup after loading the view.
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView!.reloadData()
    }

    // Mark: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Find out what row was selected
        let indexPath = self.collectionView?.indexPath(for: sender as! UICollectionViewCell)
        
        //sender as? NSIndexPath
        
        // Grab the detail view
        let detailView = (segue.destination as! UINavigationController).topViewController as! DetailViewController
        
        // Get the selected cell's image
        let bigthing = DataManager.sharedDataManager.favourites[indexPath!.row]
        
        // Pass the content to the detail view
        detailView.detailItem = bigthing
        
        // Set up navigation on detail view
        detailView.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
        detailView.navigationItem.leftItemsSupplementBackButton = true
        
        
    }
    
    // MARK: UICollectionView Data Source
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(DataManager.sharedDataManager.favourites.count)
        return DataManager.sharedDataManager.favourites.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Get an instancer of the prototype Cell we created
        guard let cell:CollectionViewCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CollectionViewCell else {
            fatalError("Wrong cell class dequeued")}
        print("set Cell here")
        cell.setCell(Bt: DataManager.sharedDataManager.favourites[indexPath.row])
        // Return the cell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 12 * 3) / 3
        let height = width * 1.5
        return CGSize(width: width, height: height)
    }
    
    // MARK: UICollectionView delegate

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }

}
