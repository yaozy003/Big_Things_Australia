//
//  SubmitController.swift
//  Bigthing3.0
//
//  Created by chenqianru520 on 27/11/19.
//  Copyright © 2019 chenqianru123. All rights reserved.
//

import UIKit

class SubmitViewController: UIViewController,UINavigationControllerDelegate,         UIImagePickerControllerDelegate{

    @IBOutlet weak var uploadImage: UIImageView!
    @IBOutlet weak var uploadName: UITextField!
    @IBOutlet weak var uploadLocation: UITextField!
    @IBOutlet weak var uploadLatitude: UITextField!
    @IBOutlet weak var uploadLongitude: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uploadImage.contentMode = UIView.ContentMode.scaleAspectFill
        uploadImage.isUserInteractionEnabled = true;
        // Do any additional setup after loading the view.
    }
    
   @IBAction func OpenCamera(sender: AnyObject)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
        var imagePicker = UIImagePickerController()
        if (UIImagePickerController.isSourceTypeAvailable(.camera)){
          imagePicker.delegate = self
          imagePicker.sourceType =
            UIImagePickerController.SourceType.camera
          imagePicker.cameraCaptureMode = .photo
          imagePicker.allowsEditing = false
          imagePicker.modalPresentationStyle = .fullScreen
          present(imagePicker, animated: true, completion: nil)
        }
      }
    }

    


}
